#include <iostream>
#include "carro.hpp"

using namespace std;

int main(int argc, char ** argv){

    Carro carro1;
    Carro carro2;

    Carro * carro3;
    carro3 = new Carro();

    Carro * carro4 = new Carro();

    carro1.set_chassis("345MHD356");
    carro1.set_fabricante("Fiat");
    carro1.set_modelo("Palio");
    carro1.set_numero_de_portas(4);
    carro1.set_cor("Prata");

    carro2.set_chassis("567LKJ234");
    carro2.set_fabricante("Ford");
    carro2.set_modelo("Ka");
    carro2.set_numero_de_portas(2);
    carro2.set_cor("Verde Limão");

    carro3->set_chassis("5ASDAS234");
    carro3->set_fabricante("Volkswagen");
    carro3->set_modelo("Gol");
    carro3->set_numero_de_portas(2);
    carro3->set_cor("Branco");


    cout << "Carro 1:" << endl;
    cout << "Chassi: " << carro1.get_chassis() << endl;
    cout << "Fabricante: " << carro1.get_fabricante() << endl;
    cout << "Modelo: " << carro1.get_modelo() << endl;
    cout << "Número de Portas: " << carro1.get_numero_de_portas() << endl;
    cout << "Cor: " << carro1.get_cor() << endl << endl;

    cout << "Carro 2:" << endl;
    cout << "Chassi: " << carro2.get_chassis() << endl;
    cout << "Fabricante: " << carro2.get_fabricante() << endl;
    cout << "Modelo: " << carro2.get_modelo() << endl;
    cout << "Número de Portas: " << carro2.get_numero_de_portas() << endl;
    cout << "Cor: " << carro2.get_cor() << endl;

    cout << "Carro 3:" << endl;
    cout << "Chassi: " << carro3->get_chassis() << endl;
    cout << "Fabricante: " << carro3->get_fabricante() << endl;
    cout << "Modelo: " << carro3->get_modelo() << endl;
    cout << "Número de Portas: " << carro3->get_numero_de_portas() << endl;
    cout << "Cor: " << carro3->get_cor() << endl;

    // movimento do Carro
    cout << endl << "Carro 4: " << endl;
    cout << "Estado: " << carro4->get_estado() << endl;
    carro4->ligar();
    cout << "Estado: " << carro4->get_estado() << endl;
    carro4->desligar();
    cout << "Estado: " << carro4->get_estado() << endl;
    carro4->ligar();
    cout << "Estado: " << carro4->get_estado() << endl;
    
    cout << "Velocidade: " << carro4->get_velocidade() << endl;
    for(int i = 0; i < 10; i++){
        carro4->acelerar(10);
        cout << "Velocidade: " << carro4->get_velocidade() << endl;
    }
    for(int i = 0; i < 10; i++){
        carro4->frear(10);
        cout << "Velocidade: " << carro4->get_velocidade() << endl;
    }
    


    delete carro3;
    delete carro4;

    return 0;

};
